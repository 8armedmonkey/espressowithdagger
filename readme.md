# Sample Usage of Espresso with Dagger and Mockito

## Enable mocking final classes

**Enable Kotlin "All-Open" compiler plugin**

In the root `build.gradle` add the following:

```groovy
buildscript {
    dependencies {
        classpath "org.jetbrains.kotlin:kotlin-allopen:$kotlin_version"
    }
}
```

The All-Open compiler plugin makes classes that are annotated with a specific annotation open. 
In this example, we will use the annotation `OpenClass` to annotate classes that we want to 
make open.

Create annotation class in `src/debug`:

```kotlin
@Target(AnnotationTarget.ANNOTATION_CLASS)
annotation class OpenClass

@OpenClass
@Target(AnnotationTarget.CLASS)
annotation class OpenForTesting
```

Create annotation class in `src/release`, this is needed to prevent the annotated classes to be 
open in the `release` build type:

```kotlin
@Target(AnnotationTarget.CLASS)
annotation class OpenForTesting
```

In the app `build.gradle` add the following:

```groovy
plugins {
    id "kotlin-allopen"
}

allOpen {
    annotation("the.path.to.OpenClass")
}
```

**Use mockito-android instead of mockito-core**

To mock Android classes, we need to use `mockito-android` instead of `mockito-core`.
Add the following Gradle dependency in the app `build.gradle`:

```groovy
androidTestImplementation "org.mockito:mockito-android:<version>"
```

## Configure Dagger for androidTest

**Enable annotation processor for androidTest**

Add Dagger dependency & enable annotation processor in app `build.gradle`:

```groovy
plugins {
    id "kotlin-kapt"
}

dependencies {
    implementation "com.google.dagger:dagger:<version>"
    kapt "com.google.dagger:dagger-compiler:<version>"
    kaptAndroidTest "com.google.dagger:dagger-compiler:<version>"
}
```

**Create custom Application class to be used in test**

Create a custom Application class `TestApplication`:

```kotlin
class TestApplication : Application(), SomeComponentProvider {

    lateinit var someComponent: SomeComponent

    override fun provideSomeComponent(): SomeComponent {
        synchronized(this) {
            if (::someComponent.isInitialized.not()) {
                someComponent = DaggerSomeComponent.builder().build()
            }
            return someComponent
        }
    }

}
```

**Replace the default AndroidJUnitRunner with a custom test runner**

Create a custom test runner class `AndroidTestRunner`:

```kotlin
class AndroidTestRunner : AndroidJUnitRunner() {

    override fun newApplication(
        cl: ClassLoader?,
        className: String?,
        context: Context?
    ): Application {
        return super.newApplication(cl, TestApplication::class.java.name, context)
    }
    
}
```

In app `build.gradle`, configure :

```groovy
testInstrumentationRunner "the.path.to.AndroidTestRunner"
```

**Configure Dagger for UI test (mocking ViewModel)**

Create a Dagger module to provide mocked ViewModel:

```kotlin
@Module
class TestViewModelModule(
    private val someViewModel: SomeViewModel = mock(SomeViewModel::class.java)
) {

    @Provides
    @IntoMap
    @ViewModelKey(SomeViewModel::class)
    fun provideSomeViewModel(): ViewModel {
        return someViewModel
    }

}
```

Add the module to the UI test Dagger component:

```kotlin
@Component(
    modules = [
        SomeModule::class,
        ViewModelFactoryModule::class,
        TestViewModelModule::class
    ]
)
@Singleton
interface UiTestComponent : SomeComponent
```

Then in the Activity UI test, use the `UiTestComponent` to inject dependencies into the Activity.

```kotlin
@RunWith(AndroidJUnit4::class)
class SomeActivityUiTest {

    private lateinit var viewModel: SomeViewModel
    private lateinit var someLiveData: MutableLiveData<String>

    @Before
    fun setUp() {
        viewModel = mock(SomeViewModel::class.java)
        someLiveData = MutableLiveData<String>()

        ApplicationProvider.getApplicationContext<TestApplication>().someComponent =
            DaggerUiTestComponent.builder()
                .testViewModelModule(TestViewModelModule(viewModel))
                .build()

        Mockito.`when`(viewModel.getSomeLiveData()).thenReturn(someLiveData)
    }

    @Test
    fun whenButtonClickedThenItShouldDisplayContent() {
        val expectedContent = "Hello UI Test"

        Mockito.doAnswer {
            someLiveData.postValue(expectedContent)
        }.`when`(viewModel).onButtonClick()

        ActivityScenario.launch(SomeActivity::class.java).use {
            onView(withId(R.id.button)).perform(click())
            onView(withId(R.id.text_view)).check(matches(withText(expectedContent)))
        }
    }

}
```

**Configure Dagger for Integration test (mocking repositories, etc.)**

Create a Dagger module to provide mocked repositories:

```kotlin
@Module
class TestSomeModule(
    private val someRepository: SomeRepository = mock(SomeRepository::class.java)
) {

    @Provides
    fun provideSomeRepository(): SomeRepository {
        return someRepository
    }

    @Provides
    fun provideSomeUseCase(someRepository: SomeRepository): SomeUseCase {
        return SomeUseCase(someRepository)
    }

}
```

Add the module to the Integration test Dagger component:

```kotlin
@Component(
    modules = [
        TestSomeModule::class,
        ViewModelFactoryModule::class,
        ViewModelModule::class
    ]
)
@Singleton
interface IntegrationTestComponent : SomeComponent
```

Then in the Activity Integration test, use the `IntegrationTestComponent` to inject dependencies into the Activity.

```kotlin
@RunWith(AndroidJUnit4::class)
class SomeActivityIntegrationTest {

    private lateinit var someRepository: SomeRepository

    @Before
    fun setUp() {
        someRepository = mock(SomeRepository::class.java)

        ApplicationProvider.getApplicationContext<TestApplication>().someComponent =
            DaggerIntegrationTestComponent.builder()
                .testSomeModule(TestSomeModule(someRepository))
                .build()
    }

    @Test
    fun whenButtonClickedThenItShouldDisplayContent() {
        val expectedContent = "Hello Integration Test"

        Mockito.`when`(someRepository.retrieveContent()).thenReturn(expectedContent)

        ActivityScenario.launch(SomeActivity::class.java).use {
            onView(withId(R.id.button)).perform(click())
            onView(withId(R.id.text_view)).check(matches(withText(expectedContent)))
        }
    }

}
```