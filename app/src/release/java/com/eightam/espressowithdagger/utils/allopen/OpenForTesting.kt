package com.eightam.espressowithdagger.utils.allopen

@Target(AnnotationTarget.CLASS)
annotation class OpenForTesting