package com.eightam.espressowithdagger.utils.di

import com.eightam.espressowithdagger.domain.settings.RetrieveSettingsUseCase
import com.eightam.espressowithdagger.domain.settings.SettingsRepository
import dagger.Module
import dagger.Provides
import org.mockito.Mockito.mock

@Module
class TestSettingsModule(
    private val settingsRepository: SettingsRepository = mock(SettingsRepository::class.java)
) {

    @Provides
    fun provideSettingsRepository(): SettingsRepository {
        return settingsRepository
    }

    @Provides
    fun provideRetrieveSettingsUseCase(settingsRepository: SettingsRepository): RetrieveSettingsUseCase {
        return RetrieveSettingsUseCase(settingsRepository)
    }

}