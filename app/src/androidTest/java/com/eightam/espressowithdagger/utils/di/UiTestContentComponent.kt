package com.eightam.espressowithdagger.utils.di

import dagger.Component
import javax.inject.Singleton

@Component(
    modules = [
        ContentModule::class,
        TestViewModelProviderFactoryModule::class,
        ViewModelModule::class
    ]
)
@Singleton
interface UiTestContentComponent : ContentComponent