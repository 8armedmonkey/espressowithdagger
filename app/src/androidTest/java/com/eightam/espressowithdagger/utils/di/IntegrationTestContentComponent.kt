package com.eightam.espressowithdagger.utils.di

import dagger.Component
import javax.inject.Singleton

@Component(
    modules = [
        TestContentModule::class,
        TestSettingsModule::class,
        ViewModelProviderFactoryModule::class,
        ViewModelModule::class
    ]
)
@Singleton
interface IntegrationTestContentComponent : ContentComponent