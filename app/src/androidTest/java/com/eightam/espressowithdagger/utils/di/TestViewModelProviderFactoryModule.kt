package com.eightam.espressowithdagger.utils.di

import dagger.Module
import dagger.Provides
import org.mockito.Mockito.mock
import javax.inject.Singleton

@Module
class TestViewModelProviderFactoryModule(
    private val viewModelProviderFactory: ViewModelProviderFactory = mock(ViewModelProviderFactory::class.java)
) {

    @Provides
    @Singleton
    fun provideViewModelProviderFactory(): ViewModelProviderFactory {
        return viewModelProviderFactory
    }

}