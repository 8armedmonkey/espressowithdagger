package com.eightam.espressowithdagger.utils.di

import com.eightam.espressowithdagger.domain.content.ContentRepository
import com.eightam.espressowithdagger.domain.content.RetrieveContentUseCase
import dagger.Module
import dagger.Provides
import org.mockito.Mockito.mock

@Module
class TestContentModule(
    private val contentRepository: ContentRepository = mock(ContentRepository::class.java)
) {

    @Provides
    fun provideContentRepository(): ContentRepository {
        return contentRepository
    }

    @Provides
    fun provideRetrieveContentUseCase(contentRepository: ContentRepository): RetrieveContentUseCase {
        return RetrieveContentUseCase(contentRepository)
    }

}