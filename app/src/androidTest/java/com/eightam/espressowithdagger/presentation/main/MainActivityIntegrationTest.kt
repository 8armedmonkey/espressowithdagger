package com.eightam.espressowithdagger.presentation.main

import androidx.test.core.app.ActivityScenario
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.eightam.espressowithdagger.R
import com.eightam.espressowithdagger.TestApplication
import com.eightam.espressowithdagger.domain.content.ContentRepository
import com.eightam.espressowithdagger.domain.settings.SettingsRepository
import com.eightam.espressowithdagger.utils.di.DaggerIntegrationTestContentComponent
import com.eightam.espressowithdagger.utils.di.TestContentModule
import com.eightam.espressowithdagger.utils.di.TestSettingsModule
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock

@RunWith(AndroidJUnit4::class)
class MainActivityIntegrationTest {

    private lateinit var contentRepository: ContentRepository
    private lateinit var settingsRepository: SettingsRepository

    @Before
    fun setUp() {
        contentRepository = mock(ContentRepository::class.java)
        settingsRepository = mock(SettingsRepository::class.java)

        ApplicationProvider.getApplicationContext<TestApplication>().contentComponent =
            DaggerIntegrationTestContentComponent.builder()
                .testContentModule(TestContentModule(contentRepository))
                .testSettingsModule(TestSettingsModule(settingsRepository))
                .build()
    }

    @Test
    fun whenContentButtonClickedThenItShouldDisplayContent() {
        val expectedContent = "Hello Integration Test"

        `when`(contentRepository.retrieveContent()).thenReturn(expectedContent)

        ActivityScenario.launch(MainActivity::class.java).use {
            onView(withId(R.id.content_button)).perform(click())
            onView(withId(R.id.content_text_view)).check(matches(withText(expectedContent)))
        }
    }

}