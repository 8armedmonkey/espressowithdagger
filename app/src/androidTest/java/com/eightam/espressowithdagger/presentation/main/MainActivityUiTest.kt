package com.eightam.espressowithdagger.presentation.main

import androidx.lifecycle.MutableLiveData
import androidx.test.core.app.ActivityScenario
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.espresso.matcher.ViewMatchers.Visibility.VISIBLE
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.eightam.espressowithdagger.R
import com.eightam.espressowithdagger.TestApplication
import com.eightam.espressowithdagger.presentation.menu.Menu
import com.eightam.espressowithdagger.presentation.menu.MenuViewModel
import com.eightam.espressowithdagger.presentation.settings.SettingsViewModel
import com.eightam.espressowithdagger.utils.di.DaggerUiTestContentComponent
import com.eightam.espressowithdagger.utils.di.TestViewModelProviderFactoryModule
import com.eightam.espressowithdagger.utils.di.ViewModelProviderFactory
import com.eightam.espressowithdagger.utils.test.atPosition
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.*

@RunWith(AndroidJUnit4::class)
class MainActivityUiTest {

    private lateinit var viewModelProviderFactory: ViewModelProviderFactory

    private lateinit var mainViewModel: MainViewModel
    private lateinit var contentLiveData: MutableLiveData<String>

    private lateinit var menuViewModel: MenuViewModel
    private lateinit var menuLiveData: MutableLiveData<Menu>

    private lateinit var settingsViewModel: SettingsViewModel
    private lateinit var settingsLiveData: MutableLiveData<List<String>>

    @Before
    fun setUp() {
        viewModelProviderFactory = mock(ViewModelProviderFactory::class.java)
        mainViewModel = mock(MainViewModel::class.java)
        contentLiveData = MutableLiveData()

        menuViewModel = mock(MenuViewModel::class.java)
        menuLiveData = MutableLiveData<Menu>()

        settingsViewModel = mock(SettingsViewModel::class.java)
        settingsLiveData = MutableLiveData()

        ApplicationProvider.getApplicationContext<TestApplication>().contentComponent =
            DaggerUiTestContentComponent.builder()
                .testViewModelProviderFactoryModule(
                    TestViewModelProviderFactoryModule(
                        viewModelProviderFactory
                    )
                )
                .build()

        `when`(viewModelProviderFactory.create(MainViewModel::class.java)).thenReturn(mainViewModel)
        `when`(viewModelProviderFactory.create(MenuViewModel::class.java)).thenReturn(menuViewModel)
        `when`(viewModelProviderFactory.create(SettingsViewModel::class.java)).thenReturn(
            settingsViewModel
        )

        `when`(mainViewModel.getContentLiveData()).thenReturn(contentLiveData)
        `when`(menuViewModel.getMenuLiveData()).thenReturn(menuLiveData)
        `when`(settingsViewModel.getSettingsLiveData()).thenReturn(settingsLiveData)

        menuLiveData.postValue(Menu.HOME)

        doAnswer {
            menuLiveData.postValue(Menu.HOME)
        }.`when`(menuViewModel).onHomeMenuItemClicked()

        doAnswer {
            menuLiveData.postValue(Menu.SETTINGS)
        }.`when`(menuViewModel).onSettingsMenuItemClicked()
    }

    @Test
    fun whenContentButtonClickedThenItShouldDisplayContent() {
        val expectedContent = "Hello UI Test"

        doAnswer {
            contentLiveData.postValue(expectedContent)
        }.`when`(mainViewModel).onButtonClick()

        ActivityScenario.launch(MainActivity::class.java).use {
            onView(withId(R.id.content_button)).perform(click())
            onView(withId(R.id.content_text_view)).check(matches(withText(expectedContent)))
        }
    }

    @Test
    fun whenHomeMenuItemIsClickedThenItShouldDisplayHome() {
        ActivityScenario.launch(MainActivity::class.java).use {
            onView(withId(R.id.home_menu_item)).perform(click())
            onView(withId(R.id.content_button)).check(matches(withEffectiveVisibility(VISIBLE)))
            onView(withId(R.id.content_text_view)).check(matches(withEffectiveVisibility(VISIBLE)))
        }
    }

    @Test
    fun whenSettingsMenuItemIsClickedThenItShouldDisplaySettings() {
        val expectedSettings = listOf("Foo", "Bar", "Baz")

        settingsLiveData.postValue(expectedSettings)

        ActivityScenario.launch(MainActivity::class.java).use {
            onView(withId(R.id.settings_menu_item)).perform(click())
            onView(withId(R.id.settings_recycler_view))
                .check(matches(withEffectiveVisibility(VISIBLE)))
            onView(withId(R.id.settings_recycler_view))
                .check(matches(atPosition(0, withText(expectedSettings[0]))))
            onView(withId(R.id.settings_recycler_view))
                .check(matches(atPosition(1, withText(expectedSettings[1]))))
            onView(withId(R.id.settings_recycler_view))
                .check(matches(atPosition(2, withText(expectedSettings[2]))))
        }
    }

}