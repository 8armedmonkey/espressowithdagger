package com.eightam.espressowithdagger.presentation.settings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.eightam.espressowithdagger.R
import com.eightam.espressowithdagger.utils.di.ContentComponentProvider
import com.eightam.espressowithdagger.utils.di.ViewModelProviderFactory
import javax.inject.Inject

class SettingsFragment : Fragment() {

    @Inject
    lateinit var viewModelProviderFactory: ViewModelProviderFactory

    lateinit var viewModel: SettingsViewModel
    lateinit var adapter: SettingsAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_settings, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        ContentComponentProvider.from(requireActivity().application).provideContentComponent().inject(this)
        super.onActivityCreated(savedInstanceState)
        setUpViewModel()
        setUpViews()
        setUpObservers()
    }

    private fun setUpViewModel() {
        viewModel = ViewModelProvider(
            requireActivity(),
            viewModelProviderFactory
        ).get(SettingsViewModel::class.java)
    }

    private fun setUpViews() {
        adapter = SettingsAdapter()
        view?.findViewById<RecyclerView>(R.id.settings_recycler_view)?.adapter = adapter
    }

    private fun setUpObservers() {
        viewModel.getSettingsLiveData().observe(viewLifecycleOwner, { settings ->
            settings?.let {
                adapter.submitList(settings)
            }
        })
    }

    companion object {

        fun newInstance(): SettingsFragment = SettingsFragment()

    }

}