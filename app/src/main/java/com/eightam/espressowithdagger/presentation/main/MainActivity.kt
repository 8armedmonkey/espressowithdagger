package com.eightam.espressowithdagger.presentation.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.eightam.espressowithdagger.R
import com.eightam.espressowithdagger.presentation.menu.Menu
import com.eightam.espressowithdagger.presentation.menu.MenuViewModel
import com.eightam.espressowithdagger.presentation.settings.SettingsFragment
import com.eightam.espressowithdagger.utils.di.ContentComponentProvider
import com.eightam.espressowithdagger.utils.di.ViewModelProviderFactory
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelProviderFactory: ViewModelProviderFactory

    lateinit var menuViewModel: MenuViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        ContentComponentProvider.from(application).provideContentComponent().inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setUpViewModel()
        setUpObserver()
    }

    private fun setUpViewModel() {
        menuViewModel = ViewModelProvider(
            this,
            viewModelProviderFactory
        ).get(MenuViewModel::class.java)
    }

    private fun setUpObserver() {
        menuViewModel.getMenuLiveData().observe(this, { menu ->
            menu?.let {
                when (it) {
                    Menu.HOME -> showHome()
                    Menu.SETTINGS -> showSettings()
                }
            }
        })
    }

    private fun showHome() {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.container, MainFragment.newInstance(), MainFragment.TAG)
            .commit()
    }

    private fun showSettings() {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.container, SettingsFragment.newInstance(), MainFragment.TAG)
            .commit()
    }

}