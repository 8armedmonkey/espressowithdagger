package com.eightam.espressowithdagger.presentation.menu

enum class Menu {
    HOME, SETTINGS
}