package com.eightam.espressowithdagger.presentation.settings

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.eightam.espressowithdagger.domain.settings.RetrieveSettingsUseCase
import com.eightam.espressowithdagger.utils.allopen.OpenForTesting
import javax.inject.Inject

@OpenForTesting
class SettingsViewModel @Inject constructor(
    private val retrieveSettingsUseCase: RetrieveSettingsUseCase
) : ViewModel() {

    private val settingsLiveData: MutableLiveData<List<String>> = MutableLiveData()

    init {
        settingsLiveData.value = retrieveSettingsUseCase.execute()
    }

    fun getSettingsLiveData(): LiveData<List<String>> = settingsLiveData

}