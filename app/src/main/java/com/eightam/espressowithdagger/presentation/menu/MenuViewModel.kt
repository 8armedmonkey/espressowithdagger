package com.eightam.espressowithdagger.presentation.menu

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.eightam.espressowithdagger.utils.allopen.OpenForTesting
import javax.inject.Inject

@OpenForTesting
class MenuViewModel @Inject constructor() : ViewModel() {

    private val menuLiveData: MutableLiveData<Menu> = MutableLiveData()

    init {
        menuLiveData.value = Menu.HOME
    }

    fun onHomeMenuItemClicked() {
        menuLiveData.value = Menu.HOME
    }

    fun onSettingsMenuItemClicked() {
        menuLiveData.value = Menu.SETTINGS
    }

    fun getMenuLiveData(): LiveData<Menu> = menuLiveData

}