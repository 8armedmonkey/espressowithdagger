package com.eightam.espressowithdagger.presentation.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.eightam.espressowithdagger.domain.content.RetrieveContentUseCase
import com.eightam.espressowithdagger.utils.allopen.OpenForTesting
import javax.inject.Inject

@OpenForTesting
class MainViewModel @Inject constructor(
    private val retrieveContentUseCase: RetrieveContentUseCase
) : ViewModel() {

    private val contentLiveData = MutableLiveData<String>()

    fun onButtonClick() {
        contentLiveData.value = retrieveContentUseCase.execute()
    }

    fun getContentLiveData(): LiveData<String> = contentLiveData

}