package com.eightam.espressowithdagger.presentation.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.eightam.espressowithdagger.R
import com.eightam.espressowithdagger.utils.di.ContentComponentProvider
import com.eightam.espressowithdagger.utils.di.ViewModelProviderFactory
import javax.inject.Inject

class MainFragment : Fragment() {

    @Inject
    lateinit var viewModelProviderFactory: ViewModelProviderFactory

    lateinit var viewModel: MainViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        ContentComponentProvider.from(requireActivity().application).provideContentComponent().inject(this)
        super.onActivityCreated(savedInstanceState)
        setUpViewModel()
        setUpViews()
        setUpObservers()
    }

    private fun setUpViewModel() {
        viewModel = ViewModelProvider(
            requireActivity(),
            viewModelProviderFactory
        ).get(MainViewModel::class.java)
    }

    private fun setUpViews() {
        view?.findViewById<Button>(R.id.content_button)?.setOnClickListener {
            viewModel.onButtonClick()
        }
    }

    private fun setUpObservers() {
        viewModel.getContentLiveData().observe(this, {
            view?.findViewById<TextView>(R.id.content_text_view)?.text = it
        })
    }

    companion object {

        val TAG = MainFragment::class.java.simpleName

        fun newInstance(): MainFragment = MainFragment()

    }

}