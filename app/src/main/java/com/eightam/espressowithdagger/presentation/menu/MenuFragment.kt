package com.eightam.espressowithdagger.presentation.menu

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.eightam.espressowithdagger.R
import com.eightam.espressowithdagger.utils.di.ContentComponentProvider
import com.eightam.espressowithdagger.utils.di.ViewModelProviderFactory
import javax.inject.Inject

class MenuFragment : Fragment() {

    @Inject
    lateinit var viewModelProviderFactory: ViewModelProviderFactory

    lateinit var viewModel: MenuViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_menu, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        ContentComponentProvider.from(requireActivity().application).provideContentComponent().inject(this)
        super.onActivityCreated(savedInstanceState)
        setUpViewModel()
        setUpViews()
        setUpObservers()
    }

    private fun setUpViewModel() {
        viewModel = ViewModelProvider(
            requireActivity(),
            viewModelProviderFactory
        ).get(MenuViewModel::class.java)
    }

    private fun setUpViews() {
        view?.findViewById<Button>(R.id.home_menu_item)?.setOnClickListener {
            viewModel.onHomeMenuItemClicked()
        }

        view?.findViewById<Button>(R.id.settings_menu_item)?.setOnClickListener {
            viewModel.onSettingsMenuItemClicked()
        }
    }

    private fun setUpObservers() {
        viewModel.getMenuLiveData().observe(viewLifecycleOwner, { menu ->
            view?.findViewById<Button>(R.id.home_menu_item)?.isSelected = Menu.HOME == menu
            view?.findViewById<Button>(R.id.settings_menu_item)?.isSelected = Menu.SETTINGS == menu
        })
    }

}