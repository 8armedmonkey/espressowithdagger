package com.eightam.espressowithdagger.utils.repository

import com.eightam.espressowithdagger.domain.content.ContentRepository

class ContentRepositoryImpl : ContentRepository {

    override fun retrieveContent(): String {
        return "Hello World!"
    }

}