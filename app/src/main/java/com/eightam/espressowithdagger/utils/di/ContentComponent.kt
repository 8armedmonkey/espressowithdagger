package com.eightam.espressowithdagger.utils.di

import com.eightam.espressowithdagger.presentation.main.MainActivity
import com.eightam.espressowithdagger.presentation.main.MainFragment
import com.eightam.espressowithdagger.presentation.menu.MenuFragment
import com.eightam.espressowithdagger.presentation.settings.SettingsFragment
import dagger.Component
import javax.inject.Singleton

@Component(
    modules = [
        ContentModule::class,
        SettingsModule::class,
        ViewModelProviderFactoryModule::class,
        ViewModelModule::class
    ]
)
@Singleton
interface ContentComponent {

    fun inject(activity: MainActivity)

    fun inject(fragment: MainFragment)

    fun inject(fragment: MenuFragment)

    fun inject(fragment: SettingsFragment)

}