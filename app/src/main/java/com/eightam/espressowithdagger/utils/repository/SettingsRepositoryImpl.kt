package com.eightam.espressowithdagger.utils.repository

import com.eightam.espressowithdagger.domain.settings.SettingsRepository

class SettingsRepositoryImpl : SettingsRepository {

    override fun retrieveSettings(): List<String> {
        return listOf("Personal Information", "Membership", "Payment Method")
    }

}