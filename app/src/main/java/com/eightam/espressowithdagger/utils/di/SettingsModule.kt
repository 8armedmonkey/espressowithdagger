package com.eightam.espressowithdagger.utils.di

import com.eightam.espressowithdagger.domain.settings.RetrieveSettingsUseCase
import com.eightam.espressowithdagger.domain.settings.SettingsRepository
import com.eightam.espressowithdagger.utils.repository.SettingsRepositoryImpl
import dagger.Module
import dagger.Provides

@Module
class SettingsModule {

    @Provides
    fun provideSettingsRepository(): SettingsRepository {
        return SettingsRepositoryImpl()
    }

    @Provides
    fun provideRetrieveSettingsUseCase(settingsRepository: SettingsRepository): RetrieveSettingsUseCase {
        return RetrieveSettingsUseCase(settingsRepository)
    }

}