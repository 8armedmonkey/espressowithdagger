package com.eightam.espressowithdagger.utils.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.eightam.espressowithdagger.utils.allopen.OpenForTesting
import javax.inject.Inject
import javax.inject.Provider
import javax.inject.Singleton

@Singleton
@OpenForTesting
class ViewModelProviderFactory @Inject constructor(
    private val creators: Map<Class<out ViewModel>, @JvmSuppressWildcards Provider<ViewModel>>
) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        val creator = creators[modelClass] ?: creators.entries.firstOrNull {
            modelClass.isAssignableFrom(it.key)
        }?.value ?: throw IllegalArgumentException("unknown model class $modelClass")
        try {
            return requireNotNull(modelClass.cast(creator.get()))
        } catch (e: Exception) {
            throw RuntimeException(e)
        }
    }
}
