package com.eightam.espressowithdagger.utils.di

import android.app.Application

interface ContentComponentProvider {

    fun provideContentComponent(): ContentComponent

    companion object {

        fun from(application: Application): ContentComponentProvider {
            if (application is ContentComponentProvider) {
                return application
            } else {
                throw RuntimeException()
            }
        }

    }

}