package com.eightam.espressowithdagger.utils.di

import com.eightam.espressowithdagger.domain.content.ContentRepository
import com.eightam.espressowithdagger.utils.repository.ContentRepositoryImpl
import com.eightam.espressowithdagger.domain.content.RetrieveContentUseCase
import dagger.Module
import dagger.Provides

@Module
class ContentModule {

    @Provides
    fun provideContentRepository(): ContentRepository {
        return ContentRepositoryImpl()
    }

    @Provides
    fun provideRetrieveContentUseCase(contentRepository: ContentRepository): RetrieveContentUseCase {
        return RetrieveContentUseCase(contentRepository)
    }

}