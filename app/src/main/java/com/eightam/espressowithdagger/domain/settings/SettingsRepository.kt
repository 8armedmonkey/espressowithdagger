package com.eightam.espressowithdagger.domain.settings

interface SettingsRepository {

    fun retrieveSettings(): List<String>

}