package com.eightam.espressowithdagger.domain.content

interface ContentRepository {

    fun retrieveContent(): String

}