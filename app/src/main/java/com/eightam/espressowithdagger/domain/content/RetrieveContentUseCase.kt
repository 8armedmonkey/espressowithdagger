package com.eightam.espressowithdagger.domain.content

class RetrieveContentUseCase(
        private val contentRepository: ContentRepository
) {

    fun execute(): String {
        return contentRepository.retrieveContent()
    }

}