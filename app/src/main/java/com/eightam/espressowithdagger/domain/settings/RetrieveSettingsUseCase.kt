package com.eightam.espressowithdagger.domain.settings

class RetrieveSettingsUseCase(
    private val settingsRepository: SettingsRepository
) {

    fun execute(): List<String> = settingsRepository.retrieveSettings()

}