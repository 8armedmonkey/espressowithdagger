package com.eightam.espressowithdagger

import android.app.Application
import com.eightam.espressowithdagger.utils.di.ContentComponent
import com.eightam.espressowithdagger.utils.di.ContentComponentProvider
import com.eightam.espressowithdagger.utils.di.DaggerContentComponent

class Application : Application(), ContentComponentProvider {

    private lateinit var contentComponent: ContentComponent

    override fun provideContentComponent(): ContentComponent {
        synchronized(this) {
            if (::contentComponent.isInitialized.not()) {
                contentComponent = DaggerContentComponent.builder().build()
            }
            return contentComponent
        }
    }

}