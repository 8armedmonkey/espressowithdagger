package com.eightam.espressowithdagger.utils.allopen

@Target(AnnotationTarget.ANNOTATION_CLASS)
annotation class OpenClass