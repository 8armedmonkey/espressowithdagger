package com.eightam.espressowithdagger.utils.allopen

@OpenClass
@Target(AnnotationTarget.CLASS)
annotation class OpenForTesting